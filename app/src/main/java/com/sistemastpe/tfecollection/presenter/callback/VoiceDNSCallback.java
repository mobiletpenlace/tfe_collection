package com.sistemastpe.tfecollection.presenter.callback;

import com.sistemastpe.tfecollection.model.DnsBean;

import io.realm.RealmList;

/**
 * Created by mahegots on 13/03/18.
 */

public interface VoiceDNSCallback {

    void onLoadListDNS(RealmList<DnsBean> dnsBeans);
}
