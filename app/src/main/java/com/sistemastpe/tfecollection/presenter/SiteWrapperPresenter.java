package com.sistemastpe.tfecollection.presenter;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.sistemastpe.tfecollection.Keys;
import com.sistemastpe.tfecollection.model.ClientesBean;
import com.sistemastpe.tfecollection.model.DireccionBean;
import com.sistemastpe.tfecollection.model.LeadStatusEntity;
import com.sistemastpe.tfecollection.model.ServiciosBean;
import com.sistemastpe.tfecollection.model.SiteEntity;
import com.sistemastpe.tfecollection.model.VozBean;

import io.realm.Realm;
import io.realm.RealmList;


abstract class SiteWrapperPresenter extends RealmPresenter {


    SiteWrapperPresenter(AppCompatActivity context) {
        super(context);
    }


    SiteEntity getSite() {
        SiteEntity entity = getRealm().where(SiteEntity.class).findFirst();
        if (entity != null) {
            return entity;
        } else {
            entity = new SiteEntity();
            entity.cliente = new ClientesBean();
            entity.cliente.servicio = new ServiciosBean();
            entity.cliente.servicio.datos = new RealmList<>();
            entity.cliente.servicio.voz = new VozBean();
            entity.cliente.servicio.voz.troncales = new RealmList<>();
            entity.cliente.servicio.voz.dns = new RealmList<>();
            entity.cliente.servicio.voz = new VozBean();
            entity.cliente.equipos = new RealmList<>();
            entity.cliente.direccion = new DireccionBean();
            SiteEntity finalEntity = entity;
            getRealm().executeTransaction(realm -> realm.copyToRealmOrUpdate(finalEntity));
            return entity;
        }
    }

    private SiteEntity getSite(String uuid) {
        return getRealm().where(SiteEntity.class).equalTo("uuid", uuid).findFirst();
    }


    private void putSite(final SiteEntity entity) {
        getRealm().executeTransaction(realm -> realm.copyToRealmOrUpdate(entity));
    }

    private void putStatusSite(final LeadStatusEntity statusEntity) {
        getRealm().executeTransaction(realm -> {
            realm.delete(LeadStatusEntity.class);
            realm.copyToRealmOrUpdate(statusEntity);
        });
    }

    private LeadStatusEntity getLeadStatus() {
        return getRealm().where(LeadStatusEntity.class).findFirst();
    }


    public SiteEntity myCurrentSite() {
        SiteEntity entity = null;
        LeadStatusEntity status = getLeadStatus();
        if (status != null) {
            if (getSite(status.uuidCurrentSite) != null)
                entity = getRealm().copyFromRealm(getSite(status.uuidCurrentSite));
        }
        if (entity == null) {
            entity = new SiteEntity();
            Log.e(Keys.DEBUG, "new entity: " + entity.uuid);
            LeadStatusEntity statusEntity = new LeadStatusEntity();
            statusEntity.stepPosition = 0;
            statusEntity.uuidCurrentSite = entity.uuid;
            putStatusSite(statusEntity);
            putSite(entity);
        }
        Log.e(Keys.DEBUG, "entity saved: " + entity.uuid);
        Log.e(Keys.DEBUG, "entity status: " + entity.status);
        Log.e(Keys.DEBUG, "entity sub: " + entity.subStatus);

        return entity;
    }

    protected boolean deleteCurrentSite() {

        final boolean[] result = {false};
        final LeadStatusEntity status = getLeadStatus();
        if (status != null) {
            getRealm().executeTransaction(realm -> {
                SiteEntity entity = realm.where(SiteEntity.class)
                        .equalTo("uuid", status.uuidCurrentSite)
                        .findFirst();
                LeadStatusEntity statusEntity = realm.where(LeadStatusEntity.class)
                        .equalTo("uuidCurrentSite", status.uuidCurrentSite)
                        .findFirst();
                assert entity != null;
                entity.deleteFromRealm();
                assert statusEntity != null;
                statusEntity.deleteFromRealm();

                result[0] = true;
            });
        }
        return result[0];
    }


    public int myCurrentSiteSubStatus() {
        return myCurrentSite().subStatus;
    }

    public void onResume() {
        super.onResume();
        if (!isGPSEnabled()) {
            if (mContext instanceof AppCompatActivity) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                ((AppCompatActivity) mContext).startActivityForResult(intent, 100);
            }
        }
    }

    private boolean isGPSEnabled() {
        LocationManager manager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        assert manager != null;
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
}
