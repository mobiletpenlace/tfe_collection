package com.sistemastpe.tfecollection.presenter;

import android.support.v7.app.AppCompatActivity;

import com.sistemastpe.tfecollection.model.ClientesBean;
import com.sistemastpe.tfecollection.model.ServiciosBean;
import com.sistemastpe.tfecollection.presenter.callback.ServiceCallback;

import io.realm.RealmList;

/**
 * Created by mahegots on 12/03/18.
 */

public class ServiceDataPresenter extends SiteWrapperPresenter {
    private final ServiceCallback callback;

    public ServiceDataPresenter(AppCompatActivity context, ServiceCallback callback) {
        super(context);
        this.callback = callback;
    }

    public void onLoadingListServices() {
        ClientesBean cliente = getSite().cliente;
        if (cliente != null) {
            ServiciosBean serviciosBean = cliente.servicio;
            callback.onLoadDataServices(serviciosBean.datos);

        } else {
            callback.onLoadDataServices(new RealmList<>());
        }
    }

}
