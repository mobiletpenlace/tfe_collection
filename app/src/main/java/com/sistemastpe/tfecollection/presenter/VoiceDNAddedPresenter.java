package com.sistemastpe.tfecollection.presenter;

import android.support.v7.app.AppCompatActivity;

import com.sistemastpe.tfecollection.model.DnsBean;
import com.sistemastpe.tfecollection.model.SiteEntity;
import com.sistemastpe.tfecollection.presenter.callback.VoiceDNAddedCallback;

import io.realm.RealmList;

/**
 * Created by mahegots on 13/03/18.
 * TeamMobileTP
 */

public class VoiceDNAddedPresenter extends SiteWrapperPresenter {
    private VoiceDNAddedCallback callback;

    public VoiceDNAddedPresenter(AppCompatActivity context, VoiceDNAddedCallback callback) {
        super(context);
        this.callback = callback;
    }

    public void saveDNSInfo(RealmList<DnsBean> dns) {
        SiteEntity entity = getSite();
        getRealm().executeTransaction(realm -> {
            entity.cliente.servicio.voz.dns = new RealmList<>();
            entity.cliente.servicio.voz.dns.clear();
            entity.cliente.servicio.voz.dns.addAll(realm.copyToRealmOrUpdate(dns));
            entity.cliente.servicio.voz.numeroLineas = dns.size();
            realm.copyToRealmOrUpdate(entity);
            callback.onSuccessDNS();
        });
    }
}
