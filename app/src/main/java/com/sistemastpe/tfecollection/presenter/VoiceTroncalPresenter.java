package com.sistemastpe.tfecollection.presenter;

import android.support.v7.app.AppCompatActivity;

import com.sistemastpe.tfecollection.presenter.callback.VoiceTroncalCallback;

/**
 * Created by mahegots on 13/03/18.
 */

public class VoiceTroncalPresenter extends SiteWrapperPresenter{
    private final VoiceTroncalCallback callback;

    public VoiceTroncalPresenter(AppCompatActivity activity, VoiceTroncalCallback callback) {
        super(activity);
        this.callback = callback;
    }

    public void onLoadingListTroncal() {
        callback.onLoadListTroncal(getSite().cliente.servicio.voz.troncales);
    }
}
