package com.sistemastpe.tfecollection.presenter;

import android.support.v7.app.AppCompatActivity;

import com.sistemastpe.tfecollection.model.IdsBean;
import com.sistemastpe.tfecollection.model.SiteEntity;
import com.sistemastpe.tfecollection.model.TroncalesBean;
import com.sistemastpe.tfecollection.presenter.callback.AddVoiceTroncalCallback;

import io.realm.RealmList;

/**
 * Created by mahegots on 13/03/18.
 * TeamMobileTP
 */

public class AddVoiceTroncalPresenter extends SiteWrapperPresenter {
    private final AddVoiceTroncalCallback callback;

    public AddVoiceTroncalPresenter(AppCompatActivity context, AddVoiceTroncalCallback callback) {
        super(context);
        this.callback = callback;
    }

    public void saveDataInfo(String troncal, RealmList<IdsBean> listIds) {
        TroncalesBean troncalesBean = new TroncalesBean();
        troncalesBean.principal = troncal;
        troncalesBean.ids = listIds;
        SiteEntity entity = getSite();
        getRealm().executeTransaction(realm -> {
            entity.cliente.servicio.voz.troncales.add(troncalesBean);
            realm.copyToRealmOrUpdate(entity);
            callback.onSuccessTroncalInfo();
        });
    }
}
