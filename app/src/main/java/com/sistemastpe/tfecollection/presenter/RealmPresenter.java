package com.sistemastpe.tfecollection.presenter;


import android.support.v7.app.AppCompatActivity;

import com.resources.mvp.BasePresenter;

import io.realm.Realm;

/**
 * Created by ivancortes on 13/02/17.
 */

abstract class RealmPresenter extends BasePresenter {

    public Realm getRealm() {
        return mRealm;
    }

    private Realm mRealm;

    RealmPresenter(AppCompatActivity context) {
        super(context);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mRealm != null && !mRealm.isClosed()) mRealm.close();
    }



}
