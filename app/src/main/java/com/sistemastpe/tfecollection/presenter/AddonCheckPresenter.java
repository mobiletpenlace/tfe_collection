package com.sistemastpe.tfecollection.presenter;

import android.location.Location;
import android.support.v7.app.AppCompatActivity;

import com.resources.utils.ConnectionUtils;
import com.sistemastpe.tfecollection.background.ws.WebServices;
import com.sistemastpe.tfecollection.model.ServiciosBean;
import com.sistemastpe.tfecollection.model.SiteEntity;
import com.sistemastpe.tfecollection.model.SiteResponse;
import com.sistemastpe.tfecollection.presenter.callback.CheckedAddonsCallback;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahegots on 14/03/18.
 * TeamMobileTP
 */

public class AddonCheckPresenter extends SiteWrapperPresenter {
    private final CheckedAddonsCallback callback;
    private Call<SiteResponse> mCallUploadSite;


    public AddonCheckPresenter(AppCompatActivity appCompatActivity, CheckedAddonsCallback callback) {
        super(appCompatActivity);
        this.callback = callback;
    }

    public void saveCheckedAddons(boolean monitoring, boolean video, boolean l2l, boolean mlps, Location lastLocation) {
        SiteEntity entity = getSite();
        getRealm().executeTransaction(realm -> {
            entity.cliente.servicio.monitoreo = monitoring;
            entity.cliente.servicio.videovigilancia = video;
            entity.cliente.servicio.l2l = l2l;
            entity.cliente.servicio.mlps = mlps;
            entity.cliente.direccion.latitud = String.valueOf(lastLocation.getLatitude());
            entity.cliente.direccion.longitud = String.valueOf(lastLocation.getLongitude());
            realm.copyToRealmOrUpdate(entity);
        });
        callback.onSuccessSavedChecked();
    }

    public void uploadSite() {
        SiteEntity entity = getRealm().copyFromRealm(getSite());
        if (ConnectionUtils.isConnected(mContext)) {
            mCallUploadSite = WebServices.services().uploadSite(entity);
            mCallUploadSite.enqueue(new Callback<SiteResponse>() {
                @Override
                public void onResponse(Call<SiteResponse> call, Response<SiteResponse> response) {
                    if (response.isSuccessful()) {
                        callback.onSuccessUploadSite();
                    } else {
                        callback.onErrorUploadSite();
                    }
                }
                @Override
                public void onFailure(Call<SiteResponse> call, Throwable t) {
                    callback.onErrorUploadSite();
                }
            });
        } else {
            callback.onErrorUploadSite();
        }
    }


    public void loadChecked() {
        ServiciosBean serviciosBean = getSite().cliente.servicio;
        callback.onSuccessChecked(serviciosBean.monitoreo, serviciosBean.videovigilancia, serviciosBean.l2l, serviciosBean.mlps);
    }

    public void clearDatabase() {
        getRealm().executeTransaction(realm -> realm.delete(SiteEntity.class));
        callback.relaunch();
    }
}
