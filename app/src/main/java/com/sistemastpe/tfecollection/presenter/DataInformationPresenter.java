package com.sistemastpe.tfecollection.presenter;

import android.support.v7.app.AppCompatActivity;

import com.sistemastpe.tfecollection.model.DatosBean;
import com.sistemastpe.tfecollection.model.IPsBean;
import com.sistemastpe.tfecollection.model.SiteEntity;
import com.sistemastpe.tfecollection.presenter.callback.DataInformationCallback;

import io.realm.RealmList;

/**
 * Created by mahegots on 13/03/18.
 */

public class DataInformationPresenter extends SiteWrapperPresenter {
    private DataInformationCallback callback;

    public DataInformationPresenter(AppCompatActivity compatActivity, DataInformationCallback callback) {
        super(compatActivity);
        this.callback = callback;
    }

    public void saveDataInfo(String serviceName, String up, String down, RealmList<IPsBean> listIps) {
        DatosBean datosBean = new DatosBean();
        datosBean.nombreServicio = serviceName;
        datosBean.anchoBandaSubida = Integer.parseInt(up);
        datosBean.anchoBandaBajada = Integer.parseInt(down);
        datosBean.ips = listIps;
        SiteEntity entity = getSite();
        getRealm().executeTransaction(realm -> {
            entity.cliente.servicio.datos.add(datosBean);
            realm.copyToRealmOrUpdate(entity);
            callback.onSuccessDataInfo();
        });
    }

}
