package com.sistemastpe.tfecollection.presenter.callback;

/**
 * Created by mahegots on 13/03/18.
 * TeamMobileTP
 */

public interface AddVoiceTroncalCallback {
    void onSuccessTroncalInfo();
}
