package com.sistemastpe.tfecollection.presenter;

import android.support.v7.app.AppCompatActivity;

import com.sistemastpe.tfecollection.presenter.callback.VoiceDNSCallback;

/**
 * Created by mahegots on 13/03/18.
 */

public class VoiceDNSPresenter extends SiteWrapperPresenter {
    private final VoiceDNSCallback callback;

    public VoiceDNSPresenter(AppCompatActivity activity, VoiceDNSCallback callback) {
        super(activity);
        this.callback = callback;
    }

    public void onLoadingListDNS() {
        callback.onLoadListDNS(getSite().cliente.servicio.voz.dns);
    }
}
