package com.sistemastpe.tfecollection.presenter.callback;

import com.sistemastpe.tfecollection.model.TroncalesBean;

import io.realm.RealmList;

/**
 * Created by mahegots on 13/03/18.
 */

public interface VoiceTroncalCallback {

    void onLoadListTroncal(RealmList<TroncalesBean> troncalesBeans);
}
