package com.sistemastpe.tfecollection.presenter;

import android.content.Context;

import com.resources.utils.Prefs;
import com.sistemastpe.tfecollection.R;

import java.security.SecureRandom;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class ApplicationPresenter {
    private Context context;

    public ApplicationPresenter(Context context) {
        this.context = context;
    }

    public void setup() {
        initRealm();
        initPrefs();
    }

    private void initPrefs() {
        Prefs.setDefaultContext(context);
    }

    private void initRealm() {
        byte[] key = new byte[64];
        new SecureRandom(key);
        Realm.init(context);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .encryptionKey(key).build();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/ProximaNova-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        Realm.setDefaultConfiguration(realmConfiguration);
    }
}
