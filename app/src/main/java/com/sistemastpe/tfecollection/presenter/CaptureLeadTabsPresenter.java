package com.sistemastpe.tfecollection.presenter;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import com.sistemastpe.tfecollection.model.LeadStatusEntity;
import com.sistemastpe.tfecollection.presenter.callback.CaptureLeadTabsCallback;
import com.sistemastpe.tfecollection.presenter.callback.ControllerTabsInterOp;

public class CaptureLeadTabsPresenter extends RealmPresenter implements TabLayout.OnTabSelectedListener, ControllerTabsInterOp {
    private final CaptureLeadTabsCallback captureLeadTabsCallback;


    public CaptureLeadTabsPresenter(AppCompatActivity context, CaptureLeadTabsCallback captureLeadTabsPresenter) {
        super(context);
        this.captureLeadTabsCallback = captureLeadTabsPresenter;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        captureLeadTabsCallback.onTabCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public int getStepPosition() {
        LeadStatusEntity leadStatus = getRealm().where(LeadStatusEntity.class).findFirst();
        return leadStatus != null ? leadStatus.stepPosition : 0;
    }

    @Override
    public void nextPageTab(int position) {
        captureLeadTabsCallback.onTabCurrentItem(position);
    }

    @Override
    public void previousPageTab(int position) {
        captureLeadTabsCallback.onTabCurrentItem(position);
    }
}
