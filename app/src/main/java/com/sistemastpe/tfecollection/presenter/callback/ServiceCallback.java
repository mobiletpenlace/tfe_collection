package com.sistemastpe.tfecollection.presenter.callback;

import com.sistemastpe.tfecollection.model.DatosBean;

import io.realm.RealmList;

/**
 * Created by mahegots on 12/03/18.
 */

public interface ServiceCallback {
    void onLoadDataServices(RealmList<DatosBean> datosBeans);

    void onSuccessSave();
}
