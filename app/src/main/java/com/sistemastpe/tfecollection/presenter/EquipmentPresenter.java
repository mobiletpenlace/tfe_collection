package com.sistemastpe.tfecollection.presenter;

import android.support.v7.app.AppCompatActivity;

import com.sistemastpe.tfecollection.model.ClientesBean;
import com.sistemastpe.tfecollection.model.EquiposBean;
import com.sistemastpe.tfecollection.model.SiteEntity;
import com.sistemastpe.tfecollection.presenter.callback.EquipmentRegisterCallback;

import io.realm.RealmList;

/**
 * Created by mahegots on 12/03/18.
 */

public class EquipmentPresenter extends SiteWrapperPresenter {
    private final EquipmentRegisterCallback callback;

    public EquipmentPresenter(AppCompatActivity activity, EquipmentRegisterCallback callback) {
        super(activity);
        this.callback = callback;
    }

    public void onLoadingListEquipments() {
        ClientesBean cliente = getSite().cliente;
        if (cliente != null) {
            RealmList<EquiposBean> equipments = cliente.equipos;
            callback.onLoadEquipments(equipments);
        } else {
            callback.onLoadEquipments(new RealmList<>());
        }
    }

    private void onSaveEquipment(RealmList<EquiposBean> equipos) {
        SiteEntity entity = getSite();
        getRealm().executeTransaction(realm -> {
            ClientesBean cliente;
            if (entity.cliente != null) {
                cliente = entity.cliente;
                cliente.equipos = equipos;

            } else {
                cliente = new ClientesBean();
                cliente.equipos = equipos;
            }
            entity.cliente = (realm.copyToRealmOrUpdate(cliente));
            realm.copyToRealmOrUpdate(entity);
            callback.onSuccessSave();
        });
    }

    public void onSaveEquipment(String marca, String modelo, String numeroSerie, String mac, RealmList<EquiposBean> equipos) {
        EquiposBean equipo = new EquiposBean();
        equipo.marca = marca;
        equipo.modelo = modelo;
        equipo.numeroSerie = numeroSerie;
        equipo.mac = mac;
        getRealm().beginTransaction();
        equipos.add(equipo);
        getRealm().commitTransaction();
        onSaveEquipment(equipos);
    }
}
