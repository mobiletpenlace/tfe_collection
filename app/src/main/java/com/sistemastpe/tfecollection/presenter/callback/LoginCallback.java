package com.sistemastpe.tfecollection.presenter.callback;


/**
 * Created by mahegots on 18/05/17.
 */

public interface LoginCallback {
    void onSuccessDeprecatedVersion(String version);

    void onSuccessLoginUser();

    void onErrorLoginUserDescription(String resultDescription);

    void onErrorQueryTicket();

 }
