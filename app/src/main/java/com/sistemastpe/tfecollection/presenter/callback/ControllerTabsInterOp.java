package com.sistemastpe.tfecollection.presenter.callback;

public interface ControllerTabsInterOp {

    void nextPageTab(int position);

    void previousPageTab(int position);
}
