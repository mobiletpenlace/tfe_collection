package com.sistemastpe.tfecollection.presenter;

import android.support.v7.app.AppCompatActivity;

import com.sistemastpe.tfecollection.model.SiteEntity;
import com.sistemastpe.tfecollection.presenter.callback.SiteInfoCallback;

/**
 * Created by mahegots on 14/03/18.
 * TeamMobileTP
 */

public class SiteInfoPresenter extends SiteWrapperPresenter {
    private final SiteInfoCallback callback;

    public SiteInfoPresenter(AppCompatActivity context, SiteInfoCallback callback) {
        super(context);
        this.callback = callback;
    }

    public void saveSiteInfo(String idSite, String nameSite, String accountSite, String ticket) {
        SiteEntity entity = getSite();

        getRealm().executeTransaction(realm -> {
            entity.idSitio = idSite;
            entity.nombreSitio = nameSite;
            entity.numeroCuenta = accountSite;
            entity.folioTracker = ticket;
            realm.copyToRealmOrUpdate(entity);
        });
    }

    public void saveUserInfo(String name, String last, String second) {
        SiteEntity entity = getSite();

        getRealm().executeTransaction(realm -> {
            entity.cliente.nombre = name;
            entity.cliente.apellidoPaterno = last;
            entity.cliente.apellidoMaterno = second;
            realm.copyToRealmOrUpdate(entity);
        });
    }

    public void saveDirectionInfo(String zipCode, String state, String colony, String street, String noExt, String noInt) {
        SiteEntity entity = getSite();
        getRealm().executeTransaction(realm -> {
            entity.cliente.direccion.cp = Integer.parseInt(zipCode);
            entity.cliente.direccion.estado = state;
            entity.cliente.direccion.colonia = colony;
            entity.cliente.direccion.calle = street;
            entity.cliente.direccion.numeroExterior = noExt;
            entity.cliente.direccion.numeroInterior = noInt;
            realm.copyToRealmOrUpdate(entity);
            callback.onSuccessDataSaved();
        });
    }

    public void loadInfo() {
        SiteEntity entity = getSite();
        callback.onSuccessLoadClient(entity.cliente, entity.idSitio, entity.nombreSitio, entity.numeroCuenta, entity.folioTracker,entity.razonSocial);

    }
}
