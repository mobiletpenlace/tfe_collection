package com.sistemastpe.tfecollection.presenter.callback;

import com.sistemastpe.tfecollection.model.EquiposBean;

import io.realm.RealmList;

/**
 * Created by mahegots on 12/03/18.
 */

public interface EquipmentRegisterCallback {
    void onLoadEquipments(RealmList<EquiposBean> equipos);

    void onSuccessSave();
}
