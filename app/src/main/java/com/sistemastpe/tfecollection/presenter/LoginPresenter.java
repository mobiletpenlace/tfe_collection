package com.sistemastpe.tfecollection.presenter;

import android.support.v7.app.AppCompatActivity;

import com.sistemastpe.tfecollection.BuildConfig;
import com.sistemastpe.tfecollection.background.ws.WebServices;
import com.sistemastpe.tfecollection.model.LoginBean;
import com.sistemastpe.tfecollection.model.VersionResponse;
import com.sistemastpe.tfecollection.presenter.callback.LoginCallback;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahegots on 18/05/17.
 */

public class LoginPresenter extends RealmPresenter {
    private LoginCallback mLoginCallback;
    private Call<VersionResponse> mAppVersionCall;
    private Call<ResponseBody> mLoginUserCall;

    public LoginPresenter(AppCompatActivity context, LoginCallback callback) {
        super(context);
        this.mLoginCallback = callback;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAppVersionCall != null && mAppVersionCall.isExecuted()) mAppVersionCall.cancel();
        if (mLoginUserCall != null && mLoginUserCall.isExecuted()) mLoginUserCall.cancel();
    }

    public void validateAppVersion() {
        mAppVersionCall = WebServices.services().appVersion("AppTFE");
        mAppVersionCall.enqueue(new Callback<VersionResponse>() {
            @Override
            public void onResponse(Call<VersionResponse> call, Response<VersionResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().version != null
                            && !response.body().version.equals(BuildConfig.VERSION_NAME)) {
                        mLoginCallback.onSuccessDeprecatedVersion(response.body().version);
                    } else {
                        mLoginCallback.onErrorQueryTicket();
                    }
                } else {
                    mLoginCallback.onErrorQueryTicket();
                }
            }

            @Override
            public void onFailure(Call<VersionResponse> call, Throwable t) {
                mLoginCallback.onErrorLoginUserDescription(t.getMessage());
            }
        });
    }

    public void loginUser(String user, String password) {
        LoginBean loginUserAppRequest = new LoginBean();
        loginUserAppRequest.username = user;
        loginUserAppRequest.password = password;
        mLoginUserCall = WebServices.services().loginUser(loginUserAppRequest);
        mLoginUserCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    mLoginCallback.onSuccessLoginUser();

                } else {
                    mLoginCallback.onErrorLoginUserDescription("Error al iniciar sesión");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mLoginCallback.onErrorLoginUserDescription(t.getMessage());
            }
        });
    }


}
