package com.sistemastpe.tfecollection.presenter.callback;

/**
 * Created by mahegots on 14/03/18.
 * TeamMobileTP
 */

public interface CheckedAddonsCallback {
    void onSuccessSavedChecked();

    void onErrorUploadSite();

    void onSuccessUploadSite();

    void onSuccessChecked(boolean monitoreo, boolean videovigilancia, boolean l2l, boolean mlps);

    void relaunch();

}
