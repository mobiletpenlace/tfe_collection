package com.sistemastpe.tfecollection.presenter.callback;

import com.sistemastpe.tfecollection.model.ClientesBean;

/**
 * Created by mahegots on 14/03/18.
 * TeamMobileTP
 */

public interface SiteInfoCallback {
    void onSuccessDataSaved();

    void onSuccessLoadClient(ClientesBean cliente, String idSitio, String nombreSitio, String numeroCuenta, String folioTracker, String razonSocial);
}
