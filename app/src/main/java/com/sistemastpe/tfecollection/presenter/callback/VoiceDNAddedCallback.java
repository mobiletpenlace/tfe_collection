package com.sistemastpe.tfecollection.presenter.callback;

/**
 * Created by mahegots on 13/03/18.
 */

public interface VoiceDNAddedCallback {
    void onSuccessDNS();
}
