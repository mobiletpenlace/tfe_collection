package com.sistemastpe.tfecollection.view.dialog;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.resources.mvp.BaseActivity;
import com.resources.mvp.BasePresenter;
import com.resources.utils.MessageUtils;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.resources.view.baseRecicler.BaseSimpleRecyclerView;
import com.sistemastpe.tfecollection.Keys;
import com.sistemastpe.tfecollection.R;
import com.sistemastpe.tfecollection.model.IPsBean;
import com.sistemastpe.tfecollection.model.IdsBean;
import com.sistemastpe.tfecollection.model.TroncalesBean;
import com.sistemastpe.tfecollection.presenter.AddVoiceTroncalPresenter;
import com.sistemastpe.tfecollection.presenter.DataInformationPresenter;
import com.sistemastpe.tfecollection.presenter.VoiceTroncalPresenter;
import com.sistemastpe.tfecollection.presenter.callback.AddVoiceTroncalCallback;
import com.sistemastpe.tfecollection.presenter.callback.VoiceTroncalCallback;
import com.sistemastpe.tfecollection.view.adapters.SimpleIDSAdapter;
import com.sistemastpe.tfecollection.view.adapters.SimpleIPAdapter;

import java.util.AbstractSequentialList;

import io.realm.Realm;
import io.realm.RealmList;

public class AddTroncalActivity extends BaseActivity implements View.OnClickListener, AddVoiceTroncalCallback {


    protected MaterialEditText textInfoTroncal;
    protected MaterialEditText textInfoIdTroncal;
    protected Button buttonAddListIdTroncal;
    protected Button buttonSaveServiceData;
    private FormValidator mFormValidator;
    private RealmList<IdsBean> listIds = new RealmList<>();
    private SimpleIDSAdapter mSimpleIDSAdapter;
    private AddVoiceTroncalPresenter mAddTroncalPresenter;

    public static Intent launch(Context context) {
        return new Intent(context, AddTroncalActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_add_troncal);
        initView();
    }

    @Override
    protected BasePresenter getPresenter() {
        return mAddTroncalPresenter = new AddVoiceTroncalPresenter(this, this);
    }


    private void initView() {
        textInfoTroncal = findViewById(R.id.text_info_troncal);
        textInfoIdTroncal = findViewById(R.id.text_info_id_troncal);
        buttonAddListIdTroncal = findViewById(R.id.button_add_list_id_troncal);
        buttonSaveServiceData = findViewById(R.id.button_save_service_data);
        buttonAddListIdTroncal.setOnClickListener(this);
        buttonSaveServiceData.setOnClickListener(this);

        new BaseSimpleRecyclerView(this, R.id.recycler_view_ids_troncal)
                .setAdapter(mSimpleIDSAdapter = new SimpleIDSAdapter());

        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(textInfoTroncal, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(textInfoIdTroncal, Regex.NOT_EMPTY, R.string.dialog_error_not_empty));

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_add_list_id_troncal) {
            if (mFormValidator.isValid()) {
                IdsBean idsBean = new IdsBean();
                idsBean.valor = textInfoIdTroncal.getText().toString();
                listIds.add(idsBean);
                textInfoIdTroncal.setText("");
                mSimpleIDSAdapter.update(listIds);
            }
        } else if (view.getId() == R.id.button_save_service_data) {
            textInfoIdTroncal.setText(R.string.phone_fake);
            if (mFormValidator.isValid()) {
                if (textInfoTroncal.getText().toString().length() == 10) {
                    mAddTroncalPresenter.saveDataInfo(textInfoTroncal.getText().toString(),
                            listIds);
                } else {
                    MessageUtils.toast(this, "Ingrese un numero de telefono valido");
                }
            }
            textInfoIdTroncal.setText("");
        }
    }


    @Override
    public void onSuccessTroncalInfo() {
        setResult(RESULT_OK);
        finish();
    }
}
