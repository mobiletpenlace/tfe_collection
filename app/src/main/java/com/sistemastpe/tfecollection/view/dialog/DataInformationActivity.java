package com.sistemastpe.tfecollection.view.dialog;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.resources.mvp.BaseActivity;
import com.resources.mvp.BasePresenter;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.resources.view.baseRecicler.BaseSimpleRecyclerView;
import com.sistemastpe.tfecollection.Keys;
import com.sistemastpe.tfecollection.R;
import com.sistemastpe.tfecollection.model.IPsBean;
import com.sistemastpe.tfecollection.presenter.DataInformationPresenter;
import com.sistemastpe.tfecollection.presenter.callback.DataInformationCallback;
import com.sistemastpe.tfecollection.view.adapters.SimpleIPAdapter;

import io.realm.RealmList;

public class DataInformationActivity extends BaseActivity implements View.OnClickListener, DataInformationCallback {

    protected MaterialEditText textInfoService;
    protected MaterialEditText textInfoUp;
    protected MaterialEditText textInfoDown;
    protected MaterialEditText textInfoIp;
    protected Button buttonAddListIps;
    protected RecyclerView recyclerViewIds;
    protected Button buttonSaveServiceData;
    private SimpleIPAdapter mSimpleItemAdapter;
    RealmList<IPsBean> listIps = new RealmList<>();
    private FormValidator mFormValidator;
    private DataInformationPresenter mDataInformationPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(true);
        setContentView(R.layout.activity_data_information);
        setTitle("Agregar servicio");
        initView();
    }

    public static Intent launch(Context context) {
        return new Intent(context, DataInformationActivity.class);
    }


    @Override
    protected BasePresenter getPresenter() {
        return mDataInformationPresenter = new DataInformationPresenter(this, this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_add_list_ips) {
            if (mFormValidator.isValid()) {
                IPsBean iPsBean = new IPsBean();
                iPsBean.valor = textInfoIp.getText().toString();
                listIps.add(iPsBean);
                textInfoIp.setText("");
                mSimpleItemAdapter.update(listIps);
            }
        } else if (view.getId() == R.id.button_save_service_data) {
            textInfoIp.setText(R.string.ip_fake);
            if (mFormValidator.isValid()) {
                mDataInformationPresenter.saveDataInfo(
                        textInfoService.getText().toString(),
                        textInfoUp.getText().toString(),
                        textInfoDown.getText().toString(), listIps);
            } else {
                textInfoIp.setText("");
            }

        }
    }

    private void initView() {
        textInfoService = findViewById(R.id.text_info_service);
        textInfoUp = findViewById(R.id.text_info_up);
        textInfoDown = findViewById(R.id.text_info_down);
        textInfoIp = findViewById(R.id.text_info_ip);

        textInfoService.setFilters(new InputFilter[]{new InputFilter.AllCaps()});


        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(textInfoService, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(textInfoUp, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(textInfoDown, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(textInfoIp, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(textInfoIp, Keys.IP_ADDRESS_PATTERN, R.string.dialog_error_ip_not_valid));


        buttonAddListIps = findViewById(R.id.button_add_list_ips);
        buttonAddListIps.setOnClickListener(DataInformationActivity.this);
        recyclerViewIds = findViewById(R.id.recycler_view_ids);
        new BaseSimpleRecyclerView(this, R.id.recycler_view_ids)
                .setAdapter(mSimpleItemAdapter = new SimpleIPAdapter());
        buttonSaveServiceData = findViewById(R.id.button_save_service_data);
        buttonSaveServiceData.setOnClickListener(DataInformationActivity.this);
    }

    @Override
    public void onSuccessDataInfo() {
        setResult(RESULT_OK);
        finish();
    }
}
