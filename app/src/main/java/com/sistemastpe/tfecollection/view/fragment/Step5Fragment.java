package com.sistemastpe.tfecollection.view.fragment;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.resources.mvp.BasePresenter;
import com.resources.utils.MessageUtils;
import com.sistemastpe.tfecollection.R;
import com.sistemastpe.tfecollection.background.services.location.LocationService;
import com.sistemastpe.tfecollection.presenter.AddonCheckPresenter;
import com.sistemastpe.tfecollection.presenter.callback.CheckedAddonsCallback;
import com.sistemastpe.tfecollection.view.activities.RegisterTFEProspect;

/**
 * Created by mahegots on 08/03/18.
 */

public class Step5Fragment extends TFEFragment implements View.OnClickListener, CheckedAddonsCallback {
    protected View rootView;
    protected AppCompatCheckBox itemCheckIsMonitored;
    protected AppCompatCheckBox itemCheckIsVideoRecording;
    protected AppCompatCheckBox itemCheckIsL2l;
    protected AppCompatCheckBox itemCheckIsMpls;
    protected Button sendInfo;
    private AddonCheckPresenter mAddonCheckPresenter;

    public static TFEFragment newInstance() {
        return new Step5Fragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        rootView = inflater.inflate(R.layout.fragment_step5, container, false);
        if (rootView != null) {
            InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
        }

        initView(rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAddonCheckPresenter.loadChecked();
    }

    @Override
    public BasePresenter getPresenter() {
        return mAddonCheckPresenter = new AddonCheckPresenter((AppCompatActivity) getContext(), this);
    }

    private void initView(View rootView) {
        itemCheckIsMonitored = rootView.findViewById(R.id.item_check_is_monitored);
        itemCheckIsVideoRecording = rootView.findViewById(R.id.item_check_is_video_recording);
        itemCheckIsL2l = rootView.findViewById(R.id.item_check_is_l2l);
        itemCheckIsMpls = rootView.findViewById(R.id.item_check_is_mpls);
        sendInfo = rootView.findViewById(R.id.send_info);
        sendInfo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        MessageUtils.progress(mActivity, "La informacion esta siendo enviada, espere un momento");

        Location lastLocation = LocationService.getLastLocation();
        if (view.getId() == R.id.send_info) {
            mAddonCheckPresenter.saveCheckedAddons(
                    itemCheckIsMonitored.isChecked(),
                    itemCheckIsVideoRecording.isChecked(),
                    itemCheckIsL2l.isChecked(),
                    itemCheckIsMpls.isChecked(), lastLocation
            );

        }
    }

    @Override
    public void onSuccessSavedChecked() {
        mAddonCheckPresenter.uploadSite();
    }

    @Override
    public void onErrorUploadSite() {

        MessageUtils.toast(getContext(), "Ocurrio un error al cargar el sitio");
        MessageUtils.stopProgress();
    }

    @Override
    public void onSuccessUploadSite() {
        MessageUtils.stopProgress();
        mAddonCheckPresenter.clearDatabase();
    }

    @Override
    public void onSuccessChecked(boolean monitoreo, boolean videovigilancia, boolean l2l, boolean mlps) {
        itemCheckIsMonitored.setChecked(monitoreo);
        itemCheckIsVideoRecording.setChecked(videovigilancia);
        itemCheckIsL2l.setChecked(l2l);
        itemCheckIsMpls.setChecked(mlps);
    }

    @Override
    public void relaunch() {
        mActivity.finish();
        mActivity.startActivity(RegisterTFEProspect.launch(mActivity));
    }
}
