package com.sistemastpe.tfecollection.view.adapters;

import android.support.v7.widget.CardView;
import android.view.View;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.resources.view.baseRecicler.BaseSimpleAdapter;
import com.resources.view.baseRecicler.BaseViewHolder;
import com.sistemastpe.tfecollection.R;
import com.sistemastpe.tfecollection.model.EquiposBean;

public class EquipmentAdapter extends BaseSimpleAdapter<EquiposBean, EquipmentAdapter.PlainViewHolder> {




    @Override
    public int getItemLayout() {
        return R.layout.item_list_equipment;
    }

    @Override
    public BaseViewHolder getViewHolder(View view) {
        return new PlainViewHolder(view);
    }


    public class PlainViewHolder extends BaseViewHolder<EquiposBean> {


        protected MaterialEditText textInfoMarc;
        protected MaterialEditText textInfoModel;
        protected MaterialEditText textInfoSerial;
        protected MaterialEditText textInfoMac;
        protected CardView contentViewItemEquipment;

        public PlainViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View rootView) {
            textInfoMarc = rootView.findViewById(R.id.text_info_marc);
            textInfoModel = rootView.findViewById(R.id.text_info_model);
            textInfoSerial = rootView.findViewById(R.id.text_info_serial);
            textInfoMac = rootView.findViewById(R.id.text_info_mac);
            contentViewItemEquipment = rootView.findViewById(R.id.content_view_item_equipment);
        }

        @Override
        public void populate(BaseViewHolder holder, int position, EquiposBean equiposBean) {
            textInfoMarc.setText(equiposBean.marca);
            textInfoModel.setText(equiposBean.modelo);
            textInfoSerial.setText(equiposBean.numeroSerie);
            textInfoMac.setText(equiposBean.mac);
        }
    }
}
