package com.sistemastpe.tfecollection.view.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.ImageView;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.resources.mvp.BaseActivity;
import com.resources.mvp.BasePresenter;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.sistemastpe.tfecollection.Keys;
import com.sistemastpe.tfecollection.R;
import com.sistemastpe.tfecollection.model.EquiposBean;
import com.sistemastpe.tfecollection.presenter.EquipmentPresenter;
import com.sistemastpe.tfecollection.presenter.callback.EquipmentRegisterCallback;
import com.sistemastpe.tfecollection.view.activities.SimpleScannerActivity;

import java.util.Locale;

import io.realm.RealmList;

public class EquipmentDialogActivity extends BaseActivity implements EquipmentRegisterCallback {

    protected MaterialEditText itemEquipmentMarc;
    protected MaterialEditText itemEquipmentModel;
    protected MaterialEditText itemEquipmentNumberSerial;
    protected MaterialEditText itemEquipmentMac;
    protected ImageView itemServiceActivationSerial;
    protected ImageView itemServiceActivationTvScanMac;
    protected ImageView itemServiceActivationTvDeleteMac;
    protected Button itemServiceAddAndSave;
    private EquipmentPresenter mEquipmentPresenter;
    private RealmList<EquiposBean> equipos;
    private FormValidator mFormValidator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_equipment_dialog);
        mEquipmentPresenter.onLoadingListEquipments();
        initView();
    }

    @Override
    protected BasePresenter getPresenter() {
        return mEquipmentPresenter = new EquipmentPresenter(this, this);
    }

    public static Intent launch(Context context) {
        return new Intent(context, EquipmentDialogActivity.class);
    }

    private void initView() {
        itemEquipmentMarc = findViewById(R.id.item_equipment_marc);
        itemEquipmentModel = findViewById(R.id.item_equipment_model);


        itemEquipmentNumberSerial = findViewById(R.id.item_equipment_number_serial);
        itemEquipmentMac = findViewById(R.id.item_equipment_mac);

        itemEquipmentMarc.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        itemEquipmentModel.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        itemEquipmentNumberSerial.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        itemEquipmentMac.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(itemEquipmentMarc, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(itemEquipmentModel, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(itemEquipmentNumberSerial, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(itemEquipmentMac, Regex.NOT_EMPTY, R.string.dialog_error_not_empty));


        itemEquipmentMac.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String address;
                if (s.length() == 12) {
                    address = formatMACAddress(itemEquipmentMac.getText().toString());
                    itemEquipmentMac.setText(address.toUpperCase(Locale.getDefault()));
                } else if ((s.length() == 17) && !(s.toString().equalsIgnoreCase(s.toString()))) {
                    address = itemEquipmentMac.getText().toString();
                    itemEquipmentMac.setText(address.toUpperCase(Locale.getDefault()));
                }
                if (s.length() > 17) {
                    address = itemEquipmentMac.getText().toString().substring(0, 16);
                    itemEquipmentMac.setText("");
                    itemEquipmentMac.append(address.toUpperCase(Locale.getDefault()));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        itemServiceActivationSerial = findViewById(R.id.item_service_activation_serial);
        itemServiceActivationTvScanMac = findViewById(R.id.item_service_activation_tv_scan_mac);
        itemServiceActivationTvDeleteMac = findViewById(R.id.item_service_activation_tv_delete_mac);

        itemServiceActivationSerial.setOnClickListener(v -> startActivityForResult(SimpleScannerActivity.createIntent(EquipmentDialogActivity.this, true, false), 500));
        itemServiceActivationTvScanMac.setOnClickListener(v -> startActivityForResult(SimpleScannerActivity.createIntent(EquipmentDialogActivity.this, false, true), 500));
        itemServiceAddAndSave = findViewById(R.id.item_service_add_and_save);
        itemServiceAddAndSave.setOnClickListener(v -> {
           if( mFormValidator.isValid()) {
               String marca = itemEquipmentMarc.getText().toString();
               String modelo = itemEquipmentModel.getText().toString();
               String numeroSerie = itemEquipmentNumberSerial.getText().toString();
               String mac = itemEquipmentMac.getText().toString();
               mEquipmentPresenter.onSaveEquipment(marca, modelo, numeroSerie, mac, equipos);
           }

        });
    }


    private String formatMACAddress(String macAddress) {
        if (!macAddress.contains(":")) {
            String insert = ":";
            int period = 2;
            int capacity = macAddress.length() + insert.length() * (macAddress.length() / period) + 1;
            StringBuilder builder = new StringBuilder(capacity);
            int index = 0;
            String prefix = "";
            while (index < macAddress.length()) {
                builder.append(prefix);
                prefix = insert;
                builder.append(macAddress.substring(index,
                        Math.min(index + period, macAddress.length())));
                index += period;
            }
            return builder.toString();
        } else {
            return macAddress.replace(":", "");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            String serialNumber = data.getExtras().getString(Keys.SCAN_RESULT);
            boolean isInternet = data.getExtras().getBoolean(Keys.EXTRA_IS_INTERNET);
            if (isInternet) {
                itemEquipmentNumberSerial.setText(serialNumber);
            } else {
                itemEquipmentMac.setText(formatMACAddress(serialNumber));
            }
        }
    }


    @Override
    public void onLoadEquipments(RealmList<EquiposBean> equipos) {
        this.equipos = equipos;
    }

    @Override
    public void onSuccessSave() {
        setResult(RESULT_OK);
        finish();
    }
}
