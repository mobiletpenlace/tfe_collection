package com.sistemastpe.tfecollection.view.dialog;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.resources.mvp.BaseActivity;
import com.resources.mvp.BasePresenter;
import com.resources.utils.MessageUtils;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.resources.view.baseRecicler.BaseSimpleRecyclerView;
import com.sistemastpe.tfecollection.R;
import com.sistemastpe.tfecollection.model.DnsBean;
import com.sistemastpe.tfecollection.presenter.VoiceDNAddedPresenter;
import com.sistemastpe.tfecollection.presenter.callback.VoiceDNAddedCallback;
import com.sistemastpe.tfecollection.view.adapters.SimpleDNSAdapter;

import io.realm.RealmList;

public class AddDNSActivity extends BaseActivity implements VoiceDNAddedCallback, View.OnClickListener {

    protected MaterialEditText textInfoDn;
    protected Button buttonAddListDns;
    protected RecyclerView recyclerViewIds;
    protected Button buttonSaveServiceDn;
    private VoiceDNAddedPresenter mVoiceDNAddedPresenter;
    private FormValidator mFormValidator;
    private SimpleDNSAdapter mSimpleAdapter;

    RealmList<DnsBean> dns = new RealmList<>();

    public static Intent launch(Context context) {
        return new Intent(context, AddDNSActivity.class);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_add_dns);
        initView();
    }

    @Override
    protected BasePresenter getPresenter() {
        return mVoiceDNAddedPresenter = new VoiceDNAddedPresenter(this, this);
    }

    private void initView() {
        textInfoDn = findViewById(R.id.text_info_dn);
        buttonAddListDns = findViewById(R.id.button_add_list_dns);
        buttonSaveServiceDn = findViewById(R.id.button_save_service_dn);
        recyclerViewIds = findViewById(R.id.recycler_view_ids);

        buttonAddListDns.setOnClickListener(this);
        buttonSaveServiceDn.setOnClickListener(this);

        mFormValidator = new FormValidator(this, true);
        mFormValidator.addValidators(
                new EditTextValidator(textInfoDn, Regex.NOT_EMPTY, R.string.dialog_error_not_empty));

        new BaseSimpleRecyclerView(this, R.id.recycler_view_dns)
                .setAdapter(mSimpleAdapter = new SimpleDNSAdapter());

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_add_list_dns) {
            if (mFormValidator.isValid()) {
                if (textInfoDn.getText().toString().length() == 10) {
                    DnsBean dnsBean = new DnsBean();
                    dnsBean.valor = textInfoDn.getText().toString();
                    dns.add(dnsBean);
                    textInfoDn.setText("");
                    mSimpleAdapter.update(dns);
                } else {
                    MessageUtils.toast(this, "Ingrese un numero de telefono valido");
                }

            }
        } else if (view.getId() == R.id.button_save_service_dn) {
            textInfoDn.setText(R.string.phone_fake);
            if (mFormValidator.isValid()) {
                mVoiceDNAddedPresenter.saveDNSInfo(dns);
            } else {
                textInfoDn.setText("");
            }
        }
    }

    @Override
    public void onSuccessDNS() {
        setResult(RESULT_OK);
        finish();
    }
}
