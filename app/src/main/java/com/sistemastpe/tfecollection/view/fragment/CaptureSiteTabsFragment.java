package com.sistemastpe.tfecollection.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.resources.mvp.BaseFragment;
import com.resources.mvp.BasePresenter;
import com.sistemastpe.tfecollection.Keys;
import com.sistemastpe.tfecollection.R;
import com.sistemastpe.tfecollection.presenter.CaptureLeadTabsPresenter;
import com.sistemastpe.tfecollection.presenter.callback.CaptureLeadTabsCallback;
import com.sistemastpe.tfecollection.view.custom.IconTextTabLayout;

import java.util.ArrayList;

public class CaptureSiteTabsFragment extends BaseFragment implements CaptureLeadTabsCallback {


    protected View rootView;
    IconTextTabLayout mTabLayout;
    ArrayList<Fragment> mFragmentList;
    String[] titles;

    CaptureLeadTabsPresenter mLeadTabsPresenter;
    private ActionBar mActionBar;


    public static Fragment newInstance() {

        return new CaptureSiteTabsFragment();
    }

    public static Fragment newInstance(int position) {
        Fragment frag = new CaptureSiteTabsFragment();
        Bundle args = new Bundle();
        args.putInt(Keys.TAB_POSITION, position);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.toolbar_custom_tabs, container, false);
        initView(rootView);
        return rootView;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        assert getActivity() != null;
        mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        mFragmentList = new ArrayList<>();
        titles = new String[]{"Datos del cliente", "Equipos", "Servicios de Datos", "Servicios de Voz", "Servicios Adicionales"};

        mFragmentList.add(Step1Fragment.newInstance().setLeadTabsPresenter(mLeadTabsPresenter));
        mFragmentList.add(Step2Fragment.newInstance().setLeadTabsPresenter(mLeadTabsPresenter));
        mFragmentList.add(Step3Fragment.newInstance().setLeadTabsPresenter(mLeadTabsPresenter));
        mFragmentList.add(Step4Fragment.newInstance().setLeadTabsPresenter(mLeadTabsPresenter));
        mFragmentList.add(Step5Fragment.newInstance());

        mTabLayout.addTab(mTabLayout.newTab().setText("1"));
        mTabLayout.addTab(mTabLayout.newTab().setText("2"));
        mTabLayout.addTab(mTabLayout.newTab().setText("3"));
        mTabLayout.addTab(mTabLayout.newTab().setText("4"));
        mTabLayout.addTab(mTabLayout.newTab().setText("5"));


        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        mTabLayout.setTabMode(TabLayout.MODE_FIXED);

        mTabLayout.addOnTabSelectedListener(mLeadTabsPresenter);
        onTabCurrentItem(0);

    }

    @Override
    public BasePresenter getPresenter() {
        return mLeadTabsPresenter = new CaptureLeadTabsPresenter((AppCompatActivity) getContext(), this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onTabCurrentItem(int currentItem) {
        if (currentItem == -1)
            currentItem = 0;
        if (mTabLayout != null) {
            TabLayout.Tab tab = mTabLayout.getTabAt(currentItem);
            assert tab != null;
            tab.select();
            if (mActionBar != null) {
                mActionBar.setTitle(titles[currentItem] != null ? titles[currentItem] : "");
            }
            FragmentActivity activity = getActivity();
            assert activity != null;
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_view,
                    mFragmentList.get(currentItem)).commit();
        }
    }

    private void initView(View rootView) {
        mTabLayout = rootView.findViewById(R.id.act_tabs_tabs);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment frag : mFragmentList) {
            frag.onActivityResult(requestCode, resultCode, data);
        }
    }
}