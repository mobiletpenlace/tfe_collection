package com.sistemastpe.tfecollection.view.custom;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.util.AttributeSet;

import com.sistemastpe.tfecollection.R;

public class IconTextTabLayout extends TabLayout {


    private int[] icons;

    public IconTextTabLayout(Context context, int[] icons) {
        super(context);
        this.icons = icons;
    }

    public IconTextTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public IconTextTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public void setTabsFromPagerAdapter(@NonNull PagerAdapter adapter) {
        this.removeAllTabs();
        int i = 0;
        for (int count = adapter.getCount(); i < count; ++i) {
            this.addTab(this.newTab().setCustomView(R.layout.toolbar_custom_tab)
                    .setIcon(icons[i])
                    .setText(adapter.getPageTitle(i)));
        }
    }
}