package com.sistemastpe.tfecollection.view.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.resources.mvp.BaseActivity;
import com.sistemastpe.tfecollection.Keys;
import com.sistemastpe.tfecollection.R;
import com.sistemastpe.tfecollection.background.services.location.LocationService;
import com.sistemastpe.tfecollection.view.fragment.CaptureSiteTabsFragment;

import java.util.ArrayList;

public class RegisterTFEProspect extends BaseActivity {

    private Fragment mCaptureSites;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(true);
        setHomeAsUpEnabled(true);
        setContentView(R.layout.activity_main);
        LocationService.requestLocation(this);
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                setFragmentContainer(mCaptureSites = CaptureSiteTabsFragment.newInstance(0), CaptureSiteTabsFragment.class.toString());
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                if (deniedPermissions != null && !deniedPermissions.isEmpty()) {
                    for (String denied : deniedPermissions) {
                        Log.e(Keys.DEBUG, denied);
                    }
                    finish();

                }

            }
        };

        new TedPermission(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("Favor de aceptar todos los permisos para continuar")
                .setPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_LOCATION_EXTRA_COMMANDS
                )
                .check();
    }

    public void setFragmentContainer(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        try {
            fragmentManager.beginTransaction()
                    .replace(R.id.main_content, fragment)
                    .commit();
        } catch (IllegalStateException e) {
            fragmentManager.beginTransaction()
                    .replace(R.id.main_content, fragment)
                    .commitAllowingStateLoss();
        }

        setTitle(title);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCaptureSites.onActivityResult(requestCode, resultCode, data);
    }

    public static Intent launch(Context context) {
        return new Intent(context, RegisterTFEProspect.class);
    }
}
