package com.sistemastpe.tfecollection.view.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.sistemastpe.tfecollection.Keys;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

public class SimpleScannerActivity extends Activity implements ZBarScannerView.ResultHandler {
    private ZBarScannerView mScannerView;
    private boolean mIsInternet;
    private boolean mIsMac;

    public static Intent createIntent(Context context, boolean isInternet, boolean isMac) {
        Intent intent = new Intent(context, SimpleScannerActivity.class);
        intent.putExtra(Keys.EXTRA_IS_INTERNET, isInternet);
        intent.putExtra(Keys.EXTRA_IS_MAC, isMac);
        return intent;
    }

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZBarScannerView(this);    // Programmatically initialize the scanner view
        mIsInternet = getIntent().getBooleanExtra(Keys.EXTRA_IS_INTERNET, true);
        mIsMac = getIntent().getBooleanExtra(Keys.EXTRA_IS_MAC, true);
        setContentView(mScannerView);                // Set the scanner view as the content view
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the response here
        Log.v(Keys.DEBUG, rawResult.getContents()); // Prints scan results
        Log.v(Keys.DEBUG, rawResult.getBarcodeFormat().getName()); // Prints the scan format (qrcode, pdf417 etc.)

        // If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);

        setResult(Activity.RESULT_OK, getIntent()
                .putExtra(Keys.SCAN_RESULT, rawResult.getContents())
                .putExtra(Keys.EXTRA_IS_INTERNET, mIsInternet)
                .putExtra(Keys.EXTRA_IS_MAC, mIsMac));
        finish();
    }
}