package com.sistemastpe.tfecollection.view.adapters;

import android.view.View;
import android.widget.TextView;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.resources.view.baseRecicler.BaseSimpleAdapter;
import com.resources.view.baseRecicler.BaseViewHolder;
import com.sistemastpe.tfecollection.R;
import com.sistemastpe.tfecollection.model.DnsBean;
import com.sistemastpe.tfecollection.model.IdsBean;
import com.sistemastpe.tfecollection.model.TroncalesBean;

/**
 * Created by mahegots on 13/03/18.
 */

public class SimpleTroncalAdapter extends BaseSimpleAdapter<TroncalesBean, SimpleTroncalAdapter.IPViewHolder> {

    @Override
    public int getItemLayout() {
        return R.layout.item_list_troncal;
    }

    @Override
    public BaseViewHolder getViewHolder(View view) {
        return new IPViewHolder(view);
    }

    class IPViewHolder extends BaseViewHolder<TroncalesBean> {
        MaterialEditText textView;
        TextView textView2;

        public IPViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text1);
            textView2 = itemView.findViewById(R.id.text2);
        }

        @Override
        public void populate(BaseViewHolder holder, int position, TroncalesBean s) {
            textView.setText(String.format("Principal: \n %s", String.valueOf(s.principal)));
            StringBuilder info = new StringBuilder();
            info.append("ids: \n");
            for (IdsBean ids : s.ids) {
                info.append(ids.valor).append("\n");
            }
            textView2.setText(info);
        }


    }
}
