package com.sistemastpe.tfecollection.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.resources.mvp.BasePresenter;
import com.resources.view.baseRecicler.BaseSimpleRecyclerView;
import com.sistemastpe.tfecollection.Keys;
import com.sistemastpe.tfecollection.R;
import com.sistemastpe.tfecollection.model.DatosBean;
import com.sistemastpe.tfecollection.presenter.ServiceDataPresenter;
import com.sistemastpe.tfecollection.presenter.callback.ServiceCallback;
import com.sistemastpe.tfecollection.view.adapters.ServiceAdapter;
import com.sistemastpe.tfecollection.view.dialog.DataInformationActivity;

import io.realm.RealmList;

import static android.app.Activity.RESULT_OK;

/**
 * Created by mahegots on 08/03/18.
 */

public class Step3Fragment extends TFEFragment implements View.OnClickListener,ServiceCallback {
    protected View rootView;
    protected RecyclerView recyclerViewServices;
    protected FloatingActionButton fabRecyclerAddItemService;
    private ServiceDataPresenter mServicesPresenter;
    private ServiceAdapter mServiceAdapter;
    protected Button validate;

    public static TFEFragment newInstance() {
        return new Step3Fragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        rootView = inflater.inflate(R.layout.fragment_step3, container, false);
        if (rootView != null) {
            InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
        }

        initView(rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mServicesPresenter.onLoadingListServices();
    }

    @Override
    public BasePresenter getPresenter() {
        return mServicesPresenter = new ServiceDataPresenter((AppCompatActivity) getContext(), this);
    }

    private void initView(View rootView) {
        recyclerViewServices = rootView.findViewById(R.id.recycler_view_services);
        recyclerViewServices.setLayoutManager(new LinearLayoutManager(getContext()));
        fabRecyclerAddItemService = rootView.findViewById(R.id.fab_recycler_add_item_service);
        new BaseSimpleRecyclerView(rootView, R.id.recycler_view_services)
                .setAdapter(mServiceAdapter = new ServiceAdapter());
        fabRecyclerAddItemService.setOnClickListener(v -> startActivityForResult(DataInformationActivity.launch(getContext()), Keys.REQUEST_LIST_DATA));
        validate = rootView.findViewById(R.id.validate);
        validate.setOnClickListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK&&requestCode==Keys.REQUEST_LIST_DATA) {
            mServicesPresenter.onLoadingListServices();
        }
    }


    @Override
    public void onLoadDataServices(RealmList<DatosBean> datosBeans) {
        mServiceAdapter.update(datosBeans);
        mServiceAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccessSave() {
        Log.e(Keys.DEBUG, "unused Method()");
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.validate) {
            getLeadTabsPresenter().nextPageTab(3);
        }
    }
}
