package com.sistemastpe.tfecollection.view.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.resources.mvp.BaseActivity;
import com.resources.mvp.BasePresenter;
import com.resources.utils.MessageUtils;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.sistemastpe.tfecollection.BuildConfig;
import com.sistemastpe.tfecollection.R;
import com.sistemastpe.tfecollection.presenter.LoginPresenter;
import com.sistemastpe.tfecollection.presenter.callback.LoginCallback;

public class LoginActivity extends BaseActivity implements LoginCallback {


    protected EditText actLoginUsername;
    protected EditText actLoginPassword;
    protected Button actLoginSignin;
    protected TextView versionTextView;
    protected ImageView previewPassword;
    private FormValidator mValidator;
    private LoginPresenter mLoginPresenter;

    public static Intent createIntent(Context applicationContext) {
        return new Intent(applicationContext, LoginActivity.class);
    }


    @Override
    protected BasePresenter getPresenter() {
        return mLoginPresenter = new LoginPresenter(this, this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        super.setContentView(R.layout.activity_login);
        initView();
    }


    private void doLogin() {
        if (mValidator.isValid()) {
            MessageUtils.progress(this, R.string.login_message);
            mLoginPresenter.loginUser(actLoginUsername.getText().toString(),
                    actLoginPassword.getText().toString());
        }
    }


    @Override
    public void onSuccessDeprecatedVersion(String version) {
        AlertDialog builder = new AlertDialog.Builder(LoginActivity.this)
                .setMessage(getString(R.string.dialog_info_new_version, version))
                .setPositiveButton("Aceptar", (dialogInterface, i) -> finish())
                .setCancelable(false)
                .create();
        builder.show();
    }

    @Override
    public void onSuccessLoginUser() {
        MessageUtils.stopProgress();
        startActivity(RegisterTFEProspect.launch(this));
    }


    @Override
    public void onErrorLoginUserDescription(String resultDescription) {
        MessageUtils.toast(this, resultDescription);
        MessageUtils.stopProgress();
    }

    @Override
    public void onErrorQueryTicket() {
        MessageUtils.stopProgress();
    }


    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        actLoginUsername = findViewById(R.id.act_login_username);
        actLoginPassword = findViewById(R.id.act_login_password);
        actLoginSignin = findViewById(R.id.act_login_signin);
        versionTextView = findViewById(R.id.act_login_version);
        previewPassword = findViewById(R.id.preview_password);

        mValidator = new FormValidator(this);
        mValidator.addValidators(
                new EditTextValidator(actLoginUsername, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(actLoginPassword, Regex.NOT_EMPTY, R.string.dialog_error_not_empty)
        );

        actLoginPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence text, int start, int before, int count) {
                if (text.length() > 5) {
                    previewPassword.setVisibility(View.VISIBLE);
                } else {
                    previewPassword.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        previewPassword.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    actLoginPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                    break;
                case MotionEvent.ACTION_UP:
                    actLoginPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    break;
            }
            return true;
        });

        actLoginPassword.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                doLogin();
            }
            return false;
        });


        actLoginSignin.setOnClickListener(v -> doLogin());
        mLoginPresenter.validateAppVersion();
        versionTextView.setText(String.format("Versión %s", BuildConfig.VERSION_NAME));


    }
}