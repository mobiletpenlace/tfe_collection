package com.sistemastpe.tfecollection.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.resources.mvp.BasePresenter;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.sistemastpe.tfecollection.R;
import com.sistemastpe.tfecollection.model.ClientesBean;
import com.sistemastpe.tfecollection.model.DireccionBean;
import com.sistemastpe.tfecollection.presenter.SiteInfoPresenter;
import com.sistemastpe.tfecollection.presenter.callback.SiteInfoCallback;

/**
 * Created by mahegots on 08/03/18.
 */

public class Step1Fragment extends TFEFragment implements View.OnClickListener, SiteInfoCallback {

    protected View rootView;
    protected MaterialEditText step1InputIdSite;
    protected MaterialEditText step1InputName;
    protected MaterialEditText step1InputLastName1;
    protected MaterialEditText step1InputLastName2;
    protected MaterialEditText step1InputZipCode;
    protected MaterialEditText step1InputState;
    protected MaterialEditText step1InputColony;
    protected MaterialEditText step1InputStreet;
    protected MaterialEditText step1InputNoExt;
    protected MaterialEditText step1InputNoInt;
    protected MaterialEditText step1NameSite;
    protected MaterialEditText step1InputAccountSite;
    protected MaterialEditText step1InputTicketTracker;
    protected Button validate;
    private FormValidator mFormValidator;
    private SiteInfoPresenter mSiteInfoPresenter;
    protected MaterialEditText step1InputSocialReason;


    public static TFEFragment newInstance() {
        return new Step1Fragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        rootView = inflater.inflate(R.layout.fragment_step1, container, false);
        if (rootView != null) {
            InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
        }

        initView(rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSiteInfoPresenter.loadInfo();
    }

    @Override
    public BasePresenter getPresenter() {
        return mSiteInfoPresenter = new SiteInfoPresenter((AppCompatActivity) getContext(), this);
    }

    private void initView(View rootView) {
        step1InputIdSite = rootView.findViewById(R.id.step1_input_id_site);
        step1InputName = rootView.findViewById(R.id.step1_input_name);
        step1InputSocialReason = rootView.findViewById(R.id.step1_input_social_reason);
        step1InputLastName1 = rootView.findViewById(R.id.step1_input_last_name1);
        step1InputLastName2 = rootView.findViewById(R.id.step1_input_last_name2);
        step1InputZipCode = rootView.findViewById(R.id.step1_input_zip_code);
        step1InputState = rootView.findViewById(R.id.step1_input_state);
        step1InputColony = rootView.findViewById(R.id.step1_input_colony);
        step1InputStreet = rootView.findViewById(R.id.step1_input_street);
        step1InputNoExt = rootView.findViewById(R.id.step1_input_no_ext);
        step1InputNoInt = rootView.findViewById(R.id.step1_input_no_int);
        step1NameSite = rootView.findViewById(R.id.step1_name_site);
        step1InputAccountSite = rootView.findViewById(R.id.step1_input_account_site);
        step1InputTicketTracker = rootView.findViewById(R.id.step1_input_ticket_tracker);

        setAllCaps(
                step1InputIdSite,
                step1NameSite,
                step1InputSocialReason,
                step1InputAccountSite,
                step1InputName,
                step1InputLastName1,
                step1InputLastName2,
                step1InputState,
                step1InputColony,
                step1InputStreet,
                step1InputNoExt,
                step1InputNoInt);

        mFormValidator = new FormValidator(getContext(), true);
        mFormValidator.addValidators(
                new EditTextValidator(step1InputIdSite, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(step1NameSite, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(step1InputSocialReason, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(step1InputName, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(step1InputLastName1, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(step1InputLastName2, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(step1InputZipCode, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(step1InputState, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(step1InputColony, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(step1InputStreet, Regex.NOT_EMPTY, R.string.dialog_error_not_empty),
                new EditTextValidator(step1InputNoExt, Regex.NOT_EMPTY, R.string.dialog_error_not_empty));
        validate = rootView.findViewById(R.id.validate);
        validate.setOnClickListener(Step1Fragment.this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.validate) {
            if (mFormValidator.isValid()) {
                mSiteInfoPresenter.saveSiteInfo(
                        step1InputIdSite.getText().toString(),
                        step1NameSite.getText().toString(),
                        step1InputAccountSite.getText().toString(),
                        step1InputTicketTracker.getText().toString());
                mSiteInfoPresenter.saveUserInfo(step1InputName.getText().toString(),
                        step1InputLastName1.getText().toString(),
                        step1InputLastName2.getText().toString());
                mSiteInfoPresenter.saveDirectionInfo(step1InputZipCode.getText().toString(),
                        step1InputState.getText().toString(),
                        step1InputColony.getText().toString(),
                        step1InputStreet.getText().toString(),
                        step1InputNoExt.getText().toString(),
                        step1InputNoInt.getText().toString());
            }
        }
    }

    @Override
    public void onSuccessDataSaved() {
        getLeadTabsPresenter().nextPageTab(1);
    }

    @Override
    public void onSuccessLoadClient(ClientesBean cliente, String idSitio, String nombreSitio, String numeroCuenta, String folioTracker, String razonSocial) {

        if (cliente != null && cliente.direccion != null) {
            DireccionBean direccionBean = cliente.direccion;

            step1InputName.setText(cliente.nombre);
            step1InputLastName1.setText(cliente.apellidoPaterno);
            step1InputLastName2.setText(cliente.apellidoMaterno);


            step1InputZipCode.setText(String.valueOf(direccionBean.cp));
            step1InputState.setText(direccionBean.estado);
            step1InputColony.setText(direccionBean.colonia);
            step1InputStreet.setText(direccionBean.calle);
            step1InputNoExt.setText(direccionBean.numeroExterior);
            step1InputNoInt.setText(direccionBean.numeroInterior);

            step1InputIdSite.setText(idSitio);
            step1NameSite.setText(nombreSitio);
            step1InputAccountSite.setText(numeroCuenta);
            step1InputTicketTracker.setText(folioTracker);
            step1InputSocialReason.setText(razonSocial);


        }
    }
}
