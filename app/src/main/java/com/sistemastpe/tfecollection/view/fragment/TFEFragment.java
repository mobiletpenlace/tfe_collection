package com.sistemastpe.tfecollection.view.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.InputFilter;
import android.widget.EditText;

import com.resources.mvp.BaseFragment;
import com.sistemastpe.tfecollection.presenter.CaptureLeadTabsPresenter;

/**
 * Created by mahegots on 08/03/18.
 */

public class TFEFragment extends BaseFragment {
    FragmentActivity mActivity;
    private CaptureLeadTabsPresenter leadTabsPresenter;

    public void setSoftInputMode(int softInputAdjustResize) {
        mActivity = getActivity();
        assert mActivity != null;
        mActivity.getWindow().setSoftInputMode(softInputAdjustResize);
    }

    public void setAllCaps(EditText... editTexts) {
        for (EditText editText : editTexts)
            editText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
    }

    public CaptureLeadTabsPresenter getLeadTabsPresenter() {
        return leadTabsPresenter;
    }

    public Fragment setLeadTabsPresenter(CaptureLeadTabsPresenter leadTabsPresenter) {
        this.leadTabsPresenter = leadTabsPresenter;
        return this;
    }
}
