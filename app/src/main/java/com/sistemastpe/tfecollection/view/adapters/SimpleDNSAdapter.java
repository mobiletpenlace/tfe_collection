package com.sistemastpe.tfecollection.view.adapters;

import android.view.View;
import android.widget.TextView;

import com.resources.view.baseRecicler.BaseSimpleAdapter;
import com.resources.view.baseRecicler.BaseViewHolder;
import com.sistemastpe.tfecollection.model.DnsBean;
import com.sistemastpe.tfecollection.model.IPsBean;

/**
 * Created by mahegots on 13/03/18.
 */

public class SimpleDNSAdapter extends BaseSimpleAdapter<DnsBean, SimpleDNSAdapter.IPViewHolder> {

    @Override
    public int getItemLayout() {
        return android.R.layout.simple_list_item_1;
    }

    @Override
    public BaseViewHolder getViewHolder(View view) {
        return new IPViewHolder(view);
    }

    class IPViewHolder extends BaseViewHolder<DnsBean> {
        TextView textView;

        public IPViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(android.R.id.text1);
        }

        @Override
        public void populate(BaseViewHolder holder, int position, DnsBean s) {
            textView.setText(s.valor);
        }


    }
}
