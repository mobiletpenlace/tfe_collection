package com.sistemastpe.tfecollection.view.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.resources.utils.Prefs;
import com.sistemastpe.tfecollection.Keys;
import com.sistemastpe.tfecollection.R;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {

    private static final long SPLASH_DELAY = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        TimerTask splashTask = new TimerTask() {
            @Override
            public void run() {
                startActivity(LoginActivity.createIntent(getApplicationContext()));
                finish();
            }
        };
        new Timer().schedule(splashTask, SPLASH_DELAY);
    }
}
