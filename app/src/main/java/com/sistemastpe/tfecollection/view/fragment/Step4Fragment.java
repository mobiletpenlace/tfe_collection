package com.sistemastpe.tfecollection.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.resources.mvp.BasePresenter;
import com.resources.view.baseRecicler.BaseSimpleRecyclerView;
import com.sistemastpe.tfecollection.Keys;
import com.sistemastpe.tfecollection.R;
import com.sistemastpe.tfecollection.model.DnsBean;
import com.sistemastpe.tfecollection.model.TroncalesBean;
import com.sistemastpe.tfecollection.presenter.VoiceDNSPresenter;
import com.sistemastpe.tfecollection.presenter.VoiceTroncalPresenter;
import com.sistemastpe.tfecollection.presenter.callback.VoiceDNSCallback;
import com.sistemastpe.tfecollection.presenter.callback.VoiceTroncalCallback;
import com.sistemastpe.tfecollection.view.adapters.SimpleDNSAdapter;
import com.sistemastpe.tfecollection.view.adapters.SimpleTroncalAdapter;
import com.sistemastpe.tfecollection.view.dialog.AddDNSActivity;
import com.sistemastpe.tfecollection.view.dialog.AddTroncalActivity;

import io.realm.RealmList;

import static android.app.Activity.RESULT_OK;

/**
 * Created by mahegots on 08/03/18.
 */

public class Step4Fragment extends TFEFragment implements View.OnClickListener, VoiceDNSCallback, VoiceTroncalCallback {
    protected View rootView;
    protected TextView typeDns;
    protected TextView typeTroncal;
    protected FloatingActionButton fabDns;
    protected RelativeLayout contentViewListDns;
    protected FloatingActionButton fabTroncal;
    protected RelativeLayout contentViewListTroncal;
    protected TextView step4LineNumbers;
    private VoiceDNSPresenter mVoiceDNSPresenter;
    private VoiceTroncalPresenter mVoiceTroncalPresenter;
    private SimpleDNSAdapter mDNSAdapter;
    private SimpleTroncalAdapter mTroncalAdapter;
    protected Button validate;


    public static TFEFragment newInstance() {
        return new Step4Fragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        rootView = inflater.inflate(R.layout.fragment_step4, container, false);
        if (rootView != null) {
            InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
        }

        initView(rootView);
        return rootView;
    }

    @Override
    public BasePresenter[] getPresenters() {
        return new BasePresenter[]{
                mVoiceDNSPresenter = new VoiceDNSPresenter((AppCompatActivity) getActivity(), this),
                mVoiceTroncalPresenter = new VoiceTroncalPresenter((AppCompatActivity) getActivity(), this)
        };
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mVoiceDNSPresenter.onLoadingListDNS();
        mVoiceTroncalPresenter.onLoadingListTroncal();
    }

    private void initView(View rootView) {
        typeDns = rootView.findViewById(R.id.type_dns);
        typeTroncal = rootView.findViewById(R.id.type_troncal);
        fabDns = rootView.findViewById(R.id.fab_dns);
        fabTroncal = rootView.findViewById(R.id.fab_troncal);
        typeDns.setOnClickListener(clickListener);
        typeTroncal.setOnClickListener(clickListener);

        fabDns.setOnClickListener(v -> startActivityForResult(AddDNSActivity.launch(getContext()), Keys.REQUEST_LIST_DNS));
        fabTroncal.setOnClickListener(v -> startActivityForResult(AddTroncalActivity.launch(getContext()), Keys.REQUEST_LIST_TRONCAL));

        new BaseSimpleRecyclerView(rootView, R.id.recycler_view_dns)
                .setLayout(new LinearLayoutManager(getContext()))
                .setAdapter(mDNSAdapter = new SimpleDNSAdapter());

        new BaseSimpleRecyclerView(rootView, R.id.recycler_view_troncal).
                setLayout(new LinearLayoutManager(getContext()))
                .setAdapter(mTroncalAdapter = new SimpleTroncalAdapter());


        contentViewListDns = rootView.findViewById(R.id.content_view_list_dns);
        contentViewListTroncal = rootView.findViewById(R.id.content_view_list_troncal);
        step4LineNumbers = rootView.findViewById(R.id.step4_line_numbers);
        validate = rootView.findViewById(R.id.validate);
        validate.setOnClickListener(this);
    }


    View.OnClickListener clickListener = view -> {
        switch (view.getId()) {
            case R.id.type_dns:
                contentViewListDns.setVisibility(View.VISIBLE);
                contentViewListTroncal.setVisibility(View.GONE);
                typeDns.setBackgroundResource(R.drawable.text_view_round_blue_a);
                typeDns.setTextColor(getResources().getColor(R.color.white));
                typeTroncal.setBackgroundResource(R.drawable.text_view_round_white_b);
                typeTroncal.setTextColor(getResources().getColor(R.color.newBlueStrone));
                break;
            case R.id.type_troncal:
                typeTroncal.setBackgroundResource(R.drawable.text_view_round_blue_a);
                typeTroncal.setTextColor(getResources().getColor(R.color.white));
                typeDns.setBackgroundResource(R.drawable.text_view_round_white_b);
                typeDns.setTextColor(getResources().getColor(R.color.newBlueStrone));
                contentViewListTroncal.setVisibility(View.VISIBLE);
                contentViewListDns.setVisibility(View.GONE);
                break;
        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Keys.REQUEST_LIST_DNS) {
            switch (requestCode) {
                case Keys.REQUEST_LIST_DNS:
                    mVoiceDNSPresenter.onLoadingListDNS();
                    break;
                case Keys.REQUEST_LIST_TRONCAL:
                    mVoiceTroncalPresenter.onLoadingListTroncal();
                    break;
            }

        }
    }

    @Override
    public void onLoadListDNS(RealmList<DnsBean> dnsBeans) {
        if (dnsBeans != null) {
            mDNSAdapter.update(dnsBeans);
            int value = dnsBeans.size();
            step4LineNumbers.setText(String.format(getString(R.string.line_numbers), String.valueOf(value)));
        }
    }

    @Override
    public void onLoadListTroncal(RealmList<TroncalesBean> troncalesBeans) {
        mTroncalAdapter.update(troncalesBeans);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.validate) {
            getLeadTabsPresenter().nextPageTab(4);
        }
    }
}
