package com.sistemastpe.tfecollection.view.adapters;

import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.resources.view.baseRecicler.BaseSimpleAdapter;
import com.resources.view.baseRecicler.BaseViewHolder;
import com.sistemastpe.tfecollection.R;
import com.sistemastpe.tfecollection.model.DatosBean;
import com.sistemastpe.tfecollection.model.IPsBean;

public class ServiceAdapter extends BaseSimpleAdapter<DatosBean, ServiceAdapter.PlainViewHolder> {


    @Override
    public int getItemLayout() {
        return R.layout.item_list_service_data;
    }

    @Override
    public BaseViewHolder getViewHolder(View view) {
        return new PlainViewHolder(view);
    }


    public class PlainViewHolder extends BaseViewHolder<DatosBean> {


        protected CardView contentViewService;
        protected MaterialEditText textInfoService;
        protected MaterialEditText textInfoUp;
        protected MaterialEditText textInfoDown;
        protected TextView textInfoIps;

        public PlainViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View rootView) {
            textInfoService = rootView.findViewById(R.id.text_info_service);
            textInfoUp = rootView.findViewById(R.id.text_info_up);
            textInfoDown = rootView.findViewById(R.id.text_info_down);
            textInfoIps = rootView.findViewById(R.id.text_info_ips);
            contentViewService = rootView.findViewById(R.id.content_view_item_service);
        }

        @Override
        public void populate(BaseViewHolder holder, int position, DatosBean datosBean) {
            textInfoService.setText(datosBean.nombreServicio);
            textInfoUp.setText(String.valueOf(datosBean.anchoBandaSubida));
            textInfoDown.setText(String.valueOf(datosBean.anchoBandaBajada));

            StringBuilder ips = new StringBuilder();
            for (IPsBean ip : datosBean.ips) {
                ips.append(ip.valor).append("\n");
            }
            textInfoIps.setText(ips.toString());
        }
    }
}
