package com.sistemastpe.tfecollection.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.resources.mvp.BasePresenter;
import com.resources.view.baseRecicler.BaseSimpleRecyclerView;
import com.sistemastpe.tfecollection.Keys;
import com.sistemastpe.tfecollection.R;
import com.sistemastpe.tfecollection.model.EquiposBean;
import com.sistemastpe.tfecollection.presenter.EquipmentPresenter;
import com.sistemastpe.tfecollection.presenter.callback.EquipmentRegisterCallback;
import com.sistemastpe.tfecollection.view.adapters.EquipmentAdapter;
import com.sistemastpe.tfecollection.view.dialog.EquipmentDialogActivity;

import io.realm.RealmList;

import static android.app.Activity.RESULT_OK;

/**
 * Created by mahegots on 08/03/18.
 */

public class Step2Fragment extends TFEFragment implements View.OnClickListener,EquipmentRegisterCallback {
    protected View rootView;
    protected RecyclerView recyclerViewEquipments;
    protected FloatingActionButton fabRecyclerAddItemEquipment;
    private EquipmentAdapter mEquipmentsAdapter;
    private EquipmentPresenter mEquipmentPresenter;
    protected Button validate;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        rootView = inflater.inflate(R.layout.fragment_step2, container, false);
        if (rootView != null) {
            InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
        }

        initView(rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mEquipmentPresenter.onLoadingListEquipments();
    }

    @Override
    public BasePresenter getPresenter() {
        return mEquipmentPresenter = new EquipmentPresenter((AppCompatActivity) getContext(), this);
    }

    public static TFEFragment newInstance() {
        return new Step2Fragment();
    }

    private void initView(View rootView) {
        recyclerViewEquipments = rootView.findViewById(R.id.recycler_view_equipments);
        recyclerViewEquipments.setLayoutManager(new LinearLayoutManager(getContext()));
        new BaseSimpleRecyclerView(rootView, R.id.recycler_view_equipments)
                .setAdapter(mEquipmentsAdapter = new EquipmentAdapter());


        fabRecyclerAddItemEquipment = rootView.findViewById(R.id.fab_recycler_add_item_equipment);
        fabRecyclerAddItemEquipment.setOnClickListener(v -> startActivityForResult(EquipmentDialogActivity.launch(getContext()), Keys.REQUEST_LIST_EQUIPMENTS));
        validate = rootView.findViewById(R.id.validate);
        validate.setOnClickListener(this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Keys.REQUEST_LIST_EQUIPMENTS) {
            mEquipmentPresenter.onLoadingListEquipments();
        }
    }

    @Override
    public void onLoadEquipments(RealmList<EquiposBean> equipos) {
        mEquipmentsAdapter.update(equipos);
        mEquipmentsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccessSave() {
        Log.e(Keys.DEBUG, "unused Method()");
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.validate) {
            getLeadTabsPresenter().nextPageTab(2);
        }
    }
}
