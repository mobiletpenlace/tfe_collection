package com.sistemastpe.tfecollection;

import android.app.Application;

import com.sistemastpe.tfecollection.presenter.ApplicationPresenter;

/**
 * Created by mahegots on 08/03/18.
 */

public class App extends Application {

    private ApplicationPresenter mApplicationPresenter;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationPresenter = new ApplicationPresenter(this);
        mApplicationPresenter.setup();
    }
}
