package com.sistemastpe.tfecollection;

/**
 * Created by mahegots on 07/03/18.
 */

public class Keys {
    public static final String DEBUG = "debug";
    public static final String TAB_POSITION = "tabPosition";
    public static final String EXTRA_IS_INTERNET = "extraInternet";
    public static final String EXTRA_IS_MAC = "extraIsMac";
    public static final String SCAN_RESULT = "scan_result";
    public static final int REQUEST_LIST_EQUIPMENTS = 500;
    public static final int REQUEST_LIST_DATA = 600;
    public static final String IP_ADDRESS_PATTERN =
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    public static final int REQUEST_LIST_DNS = 700;
    public static final int REQUEST_LIST_TRONCAL = 800;
}
