package com.sistemastpe.tfecollection.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ServiciosBean extends RealmObject{

    public ServiciosBean() {
        this.uuid = UUID.randomUUID().toString();
    }

    @PrimaryKey
    private String uuid;
    @SerializedName("datos")
    public RealmList<DatosBean> datos;
    @SerializedName("voz")
    public VozBean voz;
    @SerializedName("monitoreo")
    public boolean monitoreo;
    @SerializedName("videovigilancia")
    public boolean videovigilancia;
    @SerializedName("l2l")
    public boolean l2l;
    @SerializedName("mlps")
    public boolean mlps;

}