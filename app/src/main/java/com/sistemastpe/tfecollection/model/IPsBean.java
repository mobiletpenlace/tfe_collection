package com.sistemastpe.tfecollection.model;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class IPsBean extends RealmObject {

    public IPsBean() {
        this.uuid = UUID.randomUUID().toString();
    }

    @PrimaryKey
    private String uuid;
    @SerializedName("valor")
    public String valor;
}