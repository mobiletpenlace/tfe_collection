package com.sistemastpe.tfecollection.model;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class EquiposBean extends RealmObject {
    public EquiposBean() {
        this.uuid = UUID.randomUUID().toString();
    }

    @PrimaryKey
    private String uuid;

    @SerializedName("marca")
    public String marca;
    @SerializedName("modelo")
    public String modelo;
    @SerializedName("numero_serie")
    public String numeroSerie;
    @SerializedName("mac")
    public String mac;
}
