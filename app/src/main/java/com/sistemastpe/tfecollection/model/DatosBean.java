package com.sistemastpe.tfecollection.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DatosBean extends RealmObject {
    @SerializedName("nombre_servicio")
    public String nombreServicio;
    @SerializedName("ancho_banda_subida")
    public int anchoBandaSubida;
    @SerializedName("ancho_banda_bajada")
    public int anchoBandaBajada;
    @SerializedName("ips")
    public RealmList<IPsBean> ips;

    public DatosBean() {
        this.uuid = UUID.randomUUID().toString();
    }

    @PrimaryKey
    private String uuid;
}
