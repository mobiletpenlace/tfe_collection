package com.sistemastpe.tfecollection.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ClientesBean extends RealmObject {
    public ClientesBean() {
        this.uuid = UUID.randomUUID().toString();
    }

    @PrimaryKey
    private String uuid;
    @SerializedName("nombre")
    public String nombre;
    @SerializedName("apellido_paterno")
    public String apellidoPaterno;
    @SerializedName("apellido_materno")
    public String apellidoMaterno;
    @SerializedName("direccion")
    public DireccionBean direccion;
    @SerializedName("equipos")
    public RealmList<EquiposBean> equipos;
    @SerializedName("servicio")
    public ServiciosBean servicio;


}