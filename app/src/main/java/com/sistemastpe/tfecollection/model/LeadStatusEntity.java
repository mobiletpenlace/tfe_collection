package com.sistemastpe.tfecollection.model;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class LeadStatusEntity extends RealmObject {
    @PrimaryKey
    private String uuid;


    public String uuidCurrentSite;
    public int stepPosition = 0;

    public LeadStatusEntity() {
        this.uuid = UUID.randomUUID().toString();
    }


}
