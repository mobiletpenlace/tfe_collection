package com.sistemastpe.tfecollection.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mahegots on 14/03/18.
 * TeamMobileTP
 */

public class LoginBean {

    @SerializedName("username")
    public String username;
    @SerializedName("password")
    public String password;
}
