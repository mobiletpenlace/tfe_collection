package com.sistemastpe.tfecollection.model;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by mahegots on 07/03/18.
 */

public class SiteEntity extends RealmObject {


    public SiteEntity() {
        this.uuid = UUID.randomUUID().toString();
    }

    @PrimaryKey
    public String uuid;
    @SerializedName("nombre_sitio")
    public String nombreSitio;
    @SerializedName("numero_cuenta")
    public String numeroCuenta;
    @SerializedName("folio_tracker")
    public String folioTracker;
    @SerializedName("razon_social")
    public String razonSocial;
    @SerializedName("id_sitio")
    public String idSitio;
    @SerializedName("cliente")
    public ClientesBean cliente;


    public String status;
    public int subStatus;
}
