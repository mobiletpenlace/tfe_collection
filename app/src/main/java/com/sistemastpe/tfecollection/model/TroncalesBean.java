package com.sistemastpe.tfecollection.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class TroncalesBean extends RealmObject {
    @SerializedName("principal")
    public String principal;
    @SerializedName("ids")
    public RealmList<IdsBean> ids;

    public TroncalesBean() {
        this.uuid = UUID.randomUUID().toString();
    }

    @PrimaryKey
    private String uuid;


}