package com.sistemastpe.tfecollection.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mahegots on 14/03/18.
 * TeamMobileTP
 */

public class SiteResponse {

    @SerializedName("id_sitio")
    public String idSitio;
    @SerializedName("nombre_sitio")
    public String nombreSitio;
    @SerializedName("numero_cuenta")
    public String numeroCuenta;
    @SerializedName("folio_tracker")
    public String folioTracker;
}
