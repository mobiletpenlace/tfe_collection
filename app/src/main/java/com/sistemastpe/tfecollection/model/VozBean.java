package com.sistemastpe.tfecollection.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class VozBean extends RealmObject {

    @SerializedName("numero_lineas")
    public int numeroLineas;
    @SerializedName("dns")
    public RealmList<DnsBean> dns;
    @SerializedName("troncales")
    public RealmList<TroncalesBean> troncales;

    public VozBean() {
        this.uuid = UUID.randomUUID().toString();
    }

    @PrimaryKey
    private String uuid;



}