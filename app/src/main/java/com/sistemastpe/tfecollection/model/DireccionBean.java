package com.sistemastpe.tfecollection.model;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DireccionBean extends RealmObject {
    public DireccionBean() {
        this.uuid = UUID.randomUUID().toString();
    }

    @PrimaryKey
    private String uuid;
    @SerializedName("calle")
    public String calle;
    @SerializedName("numero_exterior")
    public String numeroExterior;
    @SerializedName("numero_interior")
    public String numeroInterior;
    @SerializedName("colonia")
    public String colonia;
    @SerializedName("cp")
    public int cp;
    @SerializedName("estado")
    public String estado;
    @SerializedName("latitud")
    public String latitud;
    @SerializedName("longitud")
    public String longitud;
}
