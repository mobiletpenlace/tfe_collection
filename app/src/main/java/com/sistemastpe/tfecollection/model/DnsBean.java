package com.sistemastpe.tfecollection.model;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DnsBean extends RealmObject {
    @SerializedName("valor")
    public String valor;
    public DnsBean() {
        this.uuid = UUID.randomUUID().toString();
    }

    @PrimaryKey
    private String uuid;


}