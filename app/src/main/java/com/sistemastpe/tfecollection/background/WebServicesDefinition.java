package com.sistemastpe.tfecollection.background;

import com.sistemastpe.tfecollection.model.LoginBean;
import com.sistemastpe.tfecollection.model.SiteEntity;
import com.sistemastpe.tfecollection.model.SiteResponse;
import com.sistemastpe.tfecollection.model.VersionResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by mahegots on 07/03/18.
 */

public interface WebServicesDefinition {

    @POST("/servicios/sitios/")
    Call<SiteResponse> uploadSite(@Body SiteEntity entity);


    @POST("/rest_framework/auth-token/")
    Call<ResponseBody> loginUser(@Body LoginBean entity);

    @GET("/api/app-version/")
    Call<VersionResponse> appVersion(@Query("app_code") String versionName);

}
