package com.sistemastpe.tfecollection.background.ws;


import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;
import com.resources.utils.GsonUtils;
import com.sistemastpe.tfecollection.BuildConfig;
import com.sistemastpe.tfecollection.background.WebServicesDefinition;

import java.util.concurrent.TimeUnit;


import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import okhttp3.internal.platform.Platform;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/***
 */

public class WebServices {
    private static WebServicesDefinition servicesDefinition;

    public static WebServicesDefinition services() {
        if (servicesDefinition == null) {
            setupServicesDefinition("");
        }
        return servicesDefinition;
    }

    public static WebServicesDefinition services(String token) {
        setupServicesDefinition(token);
        return servicesDefinition;
    }


    private static Headers getJsonHeader(String token) {
        Headers.Builder builder = new Headers.Builder();
        builder.add("Content-Type", "application/json");
        builder.add("Accept", "application/json");
        if (!token.isEmpty()) {
            builder.add("Authorization", "Token " + token);
        }
        return builder.build();
    }


    private static void setupServicesDefinition(String token) {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(120, TimeUnit.SECONDS);
        builder.connectTimeout(120, TimeUnit.SECONDS);

        builder.addInterceptor(chain -> {
            Request.Builder builder1 = chain.request().newBuilder();
            builder1.headers(getJsonHeader(token));
            return chain.proceed(builder1.build());
        });

        builder.addInterceptor(new LoggingInterceptor.Builder()
                .loggable(BuildConfig.DEBUG)
                .setLevel(Level.BASIC)
                .log(Platform.INFO)
                .request("Request")
                .response("Response")
                .addHeader("version", BuildConfig.VERSION_NAME)
                .build());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://appstotalplay.com/")
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create(GsonUtils.gsonForDeserialization()))
                .build();

        servicesDefinition = retrofit.create(WebServicesDefinition.class);
    }

}