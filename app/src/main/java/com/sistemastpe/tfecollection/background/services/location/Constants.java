package com.sistemastpe.tfecollection.background.services.location;

public class Constants {
    public static final long GOOGLE_API_CLIENT_TIMEOUT_S = 10;
    public static final String GOOGLE_API_CLIENT_ERROR_MSG =
            "Failed to connect to GoogleApiClient (error code = %d)";
    public static final String SENDER_ID = "731443648181";
    public static final String PREFS_NAME = "lnkPrefs";
    public static final String TR_SUCCESS = "correcto";
    public static final String TR_REGISTER = "registrar";
    public static final String TR_UPDATE = "actualizar";

    public static long DELAY_BACK_PRESS = 2000;


}